package scanFiles;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

/**
 * <h1>StatsViewController</h1>
 * 
 * The controller object for the stats view. 
 *
 */
public class StatsViewController {
	public StatsView view;
	public MainViewController mainCont = null;
	
	private File[] files;
	public JList<String> list;
	
	public StatsAverages avgs = null;
	
	/**
	 * void loadListData(JList, JButton)
	 * 
	 * @param list The JList object that's in the StatsView scene
	 * @param averagesButton The "Cumulative" button in the StatsView scene
	 * 
	 * This function populates the list-view with all of the "_stats.txt" files found in the ./stats/ folder
	 */
	
	// Note: this method is only called when the window is first opened
	public void loadListData(JList<String> list, JButton averagesButton) {	
		this.list = list;
		
		averagesButton.setEnabled(true);		// Sanity check
		averagesButton.setOpaque(false);
		
		DefaultListModel<String> model = new DefaultListModel<String>();	// Create a new model of Strings
		list.setModel(model);
		
		File dir = new File("./stats/");						// Search the ./stats/ directory for all .txt files
		files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		});
		
		ArrayList<String> names = new ArrayList<String>();	// Get the names of all those files without ".txt" at the end
		for (File file : files) {
			String fullName = file.getName();
			String name = fullName.replaceAll(".txt", "");
			
			names.add(name);
		}
		
		Collections.sort(names);		// Sort the names
		
		for (String name : names) {	// Add all the sorted names to the list
			model.addElement(name);
		}
	}
	
	/**
	 * void displayStats(String)
	 * 
	 * Opens a JOptionPane with the contents of the file specified by name
	 * 
	 * @param name The name of the file that we want to display (minus the ".txt" part)
	 */
	public void displayStats(String name) {
		String fileName = name + ".txt";		// The actual name of the file
		
		StringBuilder builder = new StringBuilder();	// This will parse through the file and build a string from it's contents
		
		Scanner inFile = null;
		try {
			inFile = new Scanner(new FileReader("./stats/" + fileName));	// Open the file
		} catch (FileNotFoundException ioe) {
			System.err.println(ioe);
		}

		while(inFile.hasNextLine()){		// Scan through and add each line to the builder
			String line = inFile.nextLine();
			builder.append(line);
			builder.append("\n");
		}
			
		JOptionPane.showMessageDialog(null, builder, "Stats", JOptionPane.PLAIN_MESSAGE);	// Show the resulting string (contents of file)
	}
	
	/**
	 * void clear(JList, JButton)
	 * 
	 * Clears all .txt files from ./stats/ and clears the list's model.
	 * 
	 * @param list The JList in the StatsView scene
	 * @param averagesButton The "Cumulative" JButton in the StatsView scene
	 */
	public void clear(JList<String> list, JButton averagesButton) {
		File dir = new File("./stats/");						// Get all the .txt files in the ./stats/ directory
		files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		});
		
		for (File file : files) {	// Remove all the .txt files
			file.delete();
		}
		
		File cumulativeData = new File("./StatsAverages.txt");	// Remove the cumulative data
		cumulativeData.delete();

		((DefaultListModel<String>) list.getModel()).clear();		// Clear the list
		
		avgs.clearData();										// Clear the model
		
		averagesButton.setEnabled(false);	// Disable the cumulative button since we don't have any cumulative data anymore
		averagesButton.setOpaque(true);
	}
	
	/**
	 * void reloadData()
	 * 
	 * Updates the data in the list's model. Redraws the list afterwards.
	 */
	public void reloadData() {
		// Same as loadListData()
		
		File dir = new File("./stats/");
		files = dir.listFiles(new FilenameFilter() {
			public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		});
		
		ArrayList<String> names = new ArrayList<String>();
		for (File file : files) {
			String fullName = file.getName();
			String name = fullName.replaceAll(".txt", "");
			
			names.add(name);
		}
		
		Collections.sort(names);
		
		((DefaultListModel<String>) list.getModel()).clear();
		
		for (String name : names) {
			((DefaultListModel<String>) list.getModel()).addElement(name);
		}
	}
	
	/**
	 * void closeStatsWindow()
	 * 
	 * Called when the StatsView is closed. Used by MainViewController for some state tracking.
	 */
	public void closeStatsWindow() {
		mainCont.statsWindowClosed();
		
		view.setVisible(false);	// Close the window
		view.dispose();
	}
}
