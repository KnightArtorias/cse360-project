package scanFiles;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileSystemView;

/**
 * <h1>MainViewController</h1>
 * 
 * The controller object for the MainView view 
 */
public class MainViewController {
	public static MainView view;
	
	private JButton statsButton = null;
	
	StatsAverages statsaverages = new StatsAverages();
	
	private Boolean statsOpened = false;
	private StatsView statsView = null;
	private StatsViewController statsCont = null;
	
	/**
	 * void browsePressed(JTextField, JLabel, JButton)
	 * @param dirField The text field containing the path to the selected file
	 * @param statusLabel A status label
	 * @param statsButton The stats button
	 * 
	 * Opens a file browser to the user's home directory. Once the user has selected a file, 
	 * the file is parsed (if it is a valid .txt file) and stats are created for it. If the file
	 * is not a valid .txt file, no stats will be will be created.
	 */
	public void browsePressed(JTextField dirField, JLabel statusLabel, JButton statsButton) {
		this.statsButton = statsButton;
		
		JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory()); // For Mac: /Users/userName
		int returnValue = jfc.showOpenDialog(null);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File file = jfc.getSelectedFile(); 			// File chosen from file browser
			System.out.println(file.getAbsolutePath());
			dirField.setText(file.getAbsolutePath());
			
			if (!file.getAbsolutePath().endsWith(".txt")) {	// The file chosen is not a .txt file
				statusLabel.setText("Please select a valid .txt file");
				statusLabel.setForeground(new Color(220, 20, 60));
				statusLabel.setVisible(true);
			} else {
				// Create a new scanner over the selected .txt file
				Scanner scanner = null;
				try {
					scanner = new Scanner(file);
				} catch(Exception e){
					System.out.println("Error scanning file. Error: " + e);
					return;
				}
				
				// Get the lines out of the .txt file
				ArrayList<String> lines = new ArrayList<String>();
				while(scanner.hasNextLine()) {
					lines.add(scanner.nextLine());
				}
				
				scanner.close();
				
				// Create our stats model
				Stats stats = new Stats(lines, statsaverages);
				
				// Create a new PrintWriter that writes to a .txt file called "StatsResults.txt"
				PrintWriter y = null;
				try
				{
					y = new PrintWriter(
						new FileOutputStream("./stats/" + file.getName().replaceAll(".txt", "_stats") + ".txt"));
				}
				catch(Exception e)
				{
					System.out.println("Error");
					return;  //closes the program
				}
				
				// Get today's date
				String generateDate = new SimpleDateFormat("MM-dd-yyyy").format(new Date());
				
				// Write results of the Stats object to "*name_of_file*_stats.txt"
				y.println("Stats for file: " + file.getName().replaceAll(".txt", "") + ". Generated on: " + generateDate + "\n");
				y.println("Number of lines: " + stats.getNumLines());
				y.println("Number of blank lines: " + stats.getNumBlankLines());
				y.println("Number of spaces: " + stats.getNumSpaces());
				y.println("Number of words: " + stats.getNumWords());
				y.println("Average chars per line: " + stats.getAvgCharPerLine());
				y.println("Average word length: " + stats.getAvgWordLength());
				y.println("Most common words: " + stats.getMostCommonWords());
				
				y.close();
				
				// This printer will write to the cumulative stats .txt file
				PrintWriter z = null;
				try {
					z = new PrintWriter(
						new FileOutputStream("./StatsAverages.txt"));
							
				} catch(Exception e) {
					System.out.println("Error");
					return;
				}
				
				//Write results of averages across all files to that output file
				z.println("Average across all files:\n");
				z.println("Number of lines: " + statsaverages.getNumLinesAVG());
				z.println("Number of blank lines: " + statsaverages.getNumBlankLinesAVG());
				z.println("Number of spaces: " + statsaverages.getNumSpacesAVG());
				z.println("Number of words: " + statsaverages.getNumWordsAVG());
				z.println("Average chars per line: " + statsaverages.getAvgCharPerLineAVG());
				z.println("Average word length: " + statsaverages.getAvgWordLengthAVG());
				z.println("Most common words:" + statsaverages.getMostCommonWords());
				
				z.close();
				
				// Update status label
				statusLabel.setText("Stats collected! Press stats to see.");
				statusLabel.setForeground(new Color(0, 0, 0));
				statusLabel.setVisible(true);
				
				statsButton.setEnabled(true);
				statsButton.setOpaque(false);
				
				// If the stats view is open, but not focused, update the list, then give it focus
				if (statsCont != null && statsView != null) {
					statsCont.reloadData();
					
					statsView.toFront();
					statsView.repaint();
				}
			}
		}
	}
	
	/**
	 * void statsPressed()
	 * 
	 * Opens the StatsView iff there are statistics files in the ./stats/ directory
	 */
	public void statsPressed() {
		if (!statsOpened) {	// The stats window is not open, so open it
	        statsCont = new StatsViewController();
	        statsCont.mainCont = this;
	        statsCont.avgs = statsaverages;
	        
	        statsView = new StatsView(statsCont);
	        statsView.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
	        
	        	statsView.setVisible(true);
	        	
	        	statsOpened = true;
		} else {
			if (statsView != null) {	// The stats window is open, so give it focus
				statsView.toFront();
				statsView.repaint();
			}
		}
	}
	
	/**
	 * void helpPressed()
	 * 
	 * Displays the dialog window containing the Help information. 
	 */
	public void helpPressed() {
		// Das a lotta letters...
		JOptionPane.showMessageDialog(view, 
				new JLabel("<html><body style='width: 400px'>" 														      +
						"Hello! This text files analyzer allows you to select a text file (a .txt file) from a \n" 	      + 
						"directory on your computer, and then display various statistics about that file. \n" 			  + 
						"The analyzer also supports handling multiple files, permitting you to display the \n"            + 
						"averages of the statistics across all of the text files you have selected thus far.\n"           +
						"<br><br>First, you must select a text file (a .txt file) from a directory. To do this\n" 	      + 
						", left-click the 'Browse' button on the main window. This will open a window to \n" 			  + 
						"browse your computer for the text file.<br>Once you have navigated to the \n" 				      + 
						"directory containing your text file, either double left-click the text file or \n" 			  + 
						"single left-click the text file and click the 'Open' button on the bottom right \n" 			  + 
						"of the window to select the file.<br>This window will now close, and the path to \n" 		      + 
						"your text file will now appear to the left of the 'Browse' button on the main \n" 			      + 
						"window. The analyzer will then check to see if a .txt file was selected. If a .txt \n"           +
						"was not selected, the message 'Please select a valid .txt file' will appear on the main \n"      +
						"window and nothing will be calculated. If a .txt file was selected, the statistics will \n"      +
						"be calculated and the message 'Stats collected! Press stats to see.' will appear on the \n"      +
						"main window.<br><br>Second, to display the various statistics about your text file and the \n"   +
						"averages across all text files processed thus far, left-click the 'Stats' button on the \n"      +
						"bottom right of the main window. This will open a new window. On this window \n"                 +
						"is a scroll view of all of the text files you have selected to be processed thus far as well \n" +
						"as a button labeled 'Cumulative' at the bottom of this new window.<br>To view the individual \n" +
						"statistics from any of the text files processed thus far, double left-click that file's name \n" +
						"in the scroll view. This will open a new window. At the top of this new window, the file name \n"+
						"and the date it was processed on will be displayed. Below that will be the statistics \n"        +
						"collected from that file. When you are finished viewing these statistics, either left-click \n"  +
						"the 'OK' button at the bottom of this window or left-click the X on the upper right corner.\n"   +
						"<br>To view the averages across all files processed thus far, left-click the 'Cumulative' \n"    +
						"button. This will open a new window displaying the averages of the statistics collected \n"      +
						"across all of the text files you have selected thus far. When you are finished viewing \n"       +
						"these averages, either left-click the 'OK' button at the bottom of the window or \n"             +
						"left-click the X on the upper right corner. \n"                                                  +
						"<br><br>Currently supported statistics:<br>Number of lines<br>Number \n" 	                      + 
						"of blank lines<br>Number of spaces<br>Number of words<br>Average chars per line<br>\n" 		  + 
						"Average word length<br>Most common words</body></html>"), 
			    "Help", 
			    JOptionPane.INFORMATION_MESSAGE);
	}
	
	/**
	 * void testForStats(JButton, JLabel)
	 * 
	 * This function tests if there are any .txt files in ./stats/. If there aren't, display a warning and disable the stats button
	 * 
	 * @param statsBtn The stats button from the MainView
	 * @param statusLabel The status label (lblNewLabel) from the MainView
	 */
	public void testForStats(JButton statsBtn, JLabel statusLabel) {
		File dir = new File("./stats/");	// Look in the ./stats/ folder
		Path path = dir.toPath();
		if (Files.notExists(path, LinkOption.NOFOLLOW_LINKS)) {
			dir.mkdir();
		}
		
		File[] files = dir.listFiles(new FilenameFilter() {	// And get all the .txt files in it
			public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		});
		
		ArrayList<String> names = new ArrayList<String>();	// All the file names without ".txt" at the end
		for (File file : files) {
			String fullName = file.getName();
			String name = fullName.replaceAll(".txt", "");
			
			names.add(name);
		}
		
		if (names.size() <= 0) {		// If we have results, it's good to show the stats button even if the user hasn't browsed during this launch
			statsBtn.setEnabled(false);
			statsBtn.setOpaque(true);
		}
	}
	
	/**
	 * void statsWindowClosed()
	 * 
	 * Called when the StatsView window gets closed. Does some cleanup of the state
	 */
	public void statsWindowClosed() {
		if (statsCont.list.getModel().getSize() == 0) {	// If the user cleared before closing, we need to disable the stats button
			statsButton.setEnabled(false);
			statsButton.setOpaque(true);
		}
		
		statsOpened = false;	// Update the state
	}
}
