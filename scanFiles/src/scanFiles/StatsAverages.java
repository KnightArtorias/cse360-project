package scanFiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <h1>StatsAverages.java</h1>
 * 
 * Creates a stats averages object that contains several statistics calculated based on the lines 
 * from all text files read in so far.
 * The following averages across all files read in so far can be collected:
 * 		- Number of Lines
 * 		- Number of Blank Lines
 * 		- Number of Spaces
 * 		- Number of Words
 * 		- Average Characters Per Line
 * 		- Average Word Length
 * 		- Most Common Words
 */

public class StatsAverages {
	// MARK: Members
		private int numLinesCurrFile 							= 0;
		private int numLinesTOTAL								= 0;
		private int numLinesAVG									= 0;
		private int numBlankLinesCurrFile 						= 0;
		private int numBlankLinesTOTAL 							= 0;
		private int numBlankLinesAVG 							= 0;
		private int numSpacesCurrFile 							= 0;
		private int numSpacesTOTAL 								= 0;
		private int numSpacesAVG 								= 0;
		private int numWordsCurrFile 							= 0;
		private int numWordsTOTAL 								= 0;
		private int numWordsAVG 									= 0;
		private int avgCharPerLineCurrFile 						= 0;
		private int avgCharPerLineTOTAL 							= 0;
		private int avgCharPerLineAVG 							= 0;
		private int avgWordLengthCurrFile 						= 0;
		private int avgWordLengthTOTAL 							= 0;
		private int avgWordLengthAVG 							= 0;
		private int numFilesRead									= 0;
		
		private LinkedHashMap<String, Integer> mostCommonWords 	= new LinkedHashMap<String, Integer>();
		
		/**
		 * StatsAverages
		 * 
		 * The initializer for stats averages. Calls the setters for each property.
		 */
		public StatsAverages() {	
			setNumLinesAVG(numLinesCurrFile);
			setNumBlankLinesAVG(numBlankLinesCurrFile);
			setNumSpacesAVG(numSpacesCurrFile);
			setNumWordsAVG(numWordsCurrFile);
			setAvgCharPerLineAVG(avgCharPerLineCurrFile);
			setAvgWordLengthAVG(avgWordLengthCurrFile);
		}
		
		// MARK: Getters
		/**
		 * int getNumLinesAVG()
		 * 
		 * Returns the average number of lines from all text files read so far.
		 * 
		 * @return The average number of lines from all text files read so far
		 */
		public int getNumLinesAVG() {
			return numLinesAVG;
		}
		
		/**
		 * int getNumBlankLinesAVG()
		 * 
		 * Returns the average number of blank lines from all text files read so far.
		 * 
		 * @return The average number of blank lines from all text files read so far
		 */
		public int getNumBlankLinesAVG() {
			return numBlankLinesAVG;
		}
		
		/**
		 * int getNumSpacesAVG()
		 * 
		 * Returns the average number of spaces from all text files read so far.
		 * 
		 * @return The average number of spaces from all text files read so far
		 */
		public int getNumSpacesAVG() {
			return numSpacesAVG;
		}
		
		/**
		 * int getNumWordsAVG()
		 * 
		 * Returns the average number of words from all text files read so far.
		 * 
		 * @return The average number of words from all text files read so far
		 */
		public int getNumWordsAVG() {
			return numWordsAVG;
		}
		
		/**
		 * int getAvgCharPerLineAVG()
		 * 
		 * Returns the average number of chars per line from all text files read so far.
		 * 
		 * @return The average number of chars per line from all text files read so far
		 */
		public int getAvgCharPerLineAVG() {
			return avgCharPerLineAVG;
		}
		
		/**
		 * int getAvgWordLengthAVG()
		 * 
		 * Returns the average word length from all text files read so far.
		 * 
		 * @return The average word length from all text files read so far
		 */
		public int getAvgWordLengthAVG() {
			return avgWordLengthAVG;
		}
		
		/**
		 * LinkedHashMap getMostCommonWords()
		 * 
		 * Returns the most common words from all text files read so far
		 * 
		 * @return The most common words from all text files read so far
		 */
		public LinkedHashMap<String, Integer> getMostCommonWords() {
			return mostCommonWords;
		}
		
		// MARK: Setters
		/**
		 * void setNumLinesAVG(int)
		 * @param numLinesCurrFile The numLines statistic from the current file being read
		 * 
		 * Sets the numLinesAVG property. 
		 */
		protected void setNumLinesAVG(int numLinesCurrFile) {
			if (numFilesRead == 0) {
				numLinesAVG = 0;
			}
		
			else {
				numLinesTOTAL = numLinesTOTAL + numLinesCurrFile;		// Avg num lines = total (non-blank) lines / number of files
				numLinesAVG = numLinesTOTAL / numFilesRead;
			}
		}
		
		/**
		 * void setNumBlankLinesAVG(int)
		 * @param numBlankLinesCurrFile The numBlankLines statistic from the current file being read
		 * 
		 * Sets the numBlankLinesAVG property. 
		 */
		protected void setNumBlankLinesAVG(int numBlankLinesCurrFile) {
			if (numFilesRead == 0) {
				numBlankLinesAVG = 0;
			}
			
			else {
				numBlankLinesTOTAL = numBlankLinesTOTAL + numBlankLinesCurrFile;	// Avg num lines = total (blank) lines / number of files
				numBlankLinesAVG = numBlankLinesTOTAL / numFilesRead;
			}
		}
		
		/**
		 * void setNumSpacesAVG(int)
		 * @param numSpacesCurrFile The numSpaces statistic from the current file being read
		 * 
		 * Sets the numSpacesAVG property. 
		 */
		protected void setNumSpacesAVG(int numSpacesCurrFile) {
			if (numFilesRead == 0) {
				numSpacesAVG = 0;
			}
			
			else {
				numSpacesTOTAL = numSpacesTOTAL + numSpacesCurrFile;	// Avg num spaces = total num spaces / number of files
				numSpacesAVG = numSpacesTOTAL / numFilesRead;
			}
		}
		
		/**
		 * void setNumWordsAVG(int)
		 * @param numWordsCurrFile The numWords statistic from the current file being read
		 * 
		 * Sets the numWordsAVG property. 
		 */
		protected void setNumWordsAVG(int numWordsCurrFile) {
			if (numFilesRead == 0) {
				numWordsAVG = 0;
			}
			
			else {
				numWordsTOTAL = numWordsTOTAL + numWordsCurrFile;		// Avg num words = total num of words / number of files
				numWordsAVG = numWordsTOTAL / numFilesRead;
			}
		}
		
		/**
		 * void setAvgCharPerLineAVG(int)
		 * @param avgCharPerLineCurrFile The avgCharPerLine statistic from the current file being read
		 * 
		 * Sets the avgCharPerLineAVG property. 
		 */
		protected void setAvgCharPerLineAVG(int avgCharPerLineCurrFile) {
			if (numFilesRead == 0) {
				avgCharPerLineAVG = 0;
			}
			
			else {
				avgCharPerLineTOTAL = avgCharPerLineTOTAL + avgCharPerLineCurrFile;	// Avg avg char per line = total avg char per line / number of files
				avgCharPerLineAVG = avgCharPerLineTOTAL / numFilesRead;
			}
		}
		
		/**
		 * void setAvgWordLengthAVG(int)
		 * @param avgWordLengthCurrFile The avgWordLength statistic from the current file being read
		 * 
		 * Sets the avgWordLengthAVG property. 
		 */
		protected void setAvgWordLengthAVG(int avgWordLengthCurrFile) {
			if (numFilesRead == 0) {
				avgWordLengthAVG = 0;
			}
			
			else {
				avgWordLengthTOTAL = avgWordLengthTOTAL + avgWordLengthCurrFile;	// Avg avg word length = total avg word length / number of files
				avgWordLengthAVG = avgWordLengthTOTAL / numFilesRead;
			}
		}
		
		/**
		 * setMostCommonWords(ArrayList)
		 * 
		 * Sets the mostCommonWords statistic from the current file being read
		 * 
		 * @param lines The lines in the input file
		 */
		protected void setMostCommonWords(ArrayList<String> lines) {	
			for (String line : lines) {
				line = line.toLowerCase();
				if (line.hashCode() == 0) {	// Blank line, skip
					continue;
				}
				
				String words[] = line.split(" ");	// Get all the words
				
				for (String word : words) {
					if (mostCommonWords.containsKey(word)) {		// If the word is in the hashMap
						mostCommonWords.put(word, mostCommonWords.get(word) + 1);		// Increase it's value by 1
					} else {
						mostCommonWords.put(word, 1);	// Otherwise add a new entry with initial value of 1
					}
				}
			}
			
			mostCommonWords = (LinkedHashMap<String, Integer>) sortByValue(mostCommonWords);	// Sort em
		}
		
		/**
		 * void incrementNumFilesRead()
		 * 
		 * 
		 * Increments the number of text files that have been read thus far. 
		 */
		protected void incrementNumFilesRead() {
			numFilesRead++;
		}
		
		/**
		 * sortByValue(Map)
		 * @param map The map we want to sort on
		 * @param <K> The keys in the map
		 * @param <V> The values in the map
		 * @return A map containing the original data sorted by value
		 * 
		 * Generic sort for ordered Maps on their values. Values must be comparable. Entries must be ordered (i.e. LinkedHashMap)
		 */
		public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
			// Functionally, this operation will break down the Map into it's component parts by using a functional map, 
			// then, using a collector, to collector, collects the elements back up in "reverse-sorted" order (descending). 
			// We specify that we want to sort on V (values).
			
			// It is probably easiest to read this from bottom to top
		    return map.entrySet()	// Gives a set
		              .stream()		// Get a stream of the set so we can use sorted
		              .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))	// reverseOrder is in descending order
		              .collect(Collectors.toMap(	// Standard functional map. Collect will collect the results and store them in the LinkedHashMap
		                Map.Entry::getKey, 		// Keys
		                Map.Entry::getValue, 	// Values
		                (e1, e2) -> e1, 			// Merge function (resolves collisions)
		                LinkedHashMap::new		// Return
		              ));
		    
		    // toMap breaks down the Map into Keys and Values and returns a Collector
		    // Collect will perform a reduction on the Collector (this removes duplicates)
		    // Sorted will take the result of the reduction and sort it in descending order by value
		    // stream() and entrySet() are there to convert to the appropriate type
		}
		
		/**
		 * void clearData()
		 * 
		 * Clears all the data
		 */
		public void clearData() {
			 numLinesCurrFile 							= 0;
			 numLinesTOTAL								= 0;
			 numLinesAVG									= 0;
			 numBlankLinesCurrFile 						= 0;
			 numBlankLinesTOTAL 							= 0;
			 numBlankLinesAVG 							= 0;
			 numSpacesCurrFile 							= 0;
			 numSpacesTOTAL 								= 0;
			 numSpacesAVG 								= 0;
			 numWordsCurrFile 							= 0;
			 numWordsTOTAL 								= 0;
			 numWordsAVG 								= 0;
			 avgCharPerLineCurrFile 						= 0;
			 avgCharPerLineTOTAL 						= 0;
			 avgCharPerLineAVG 							= 0;
			 avgWordLengthCurrFile 						= 0;
			 avgWordLengthTOTAL 							= 0;
			 avgWordLengthAVG 							= 0;
			 numFilesRead								= 0;
			 mostCommonWords 							= new LinkedHashMap<String, Integer>();
		}
}
