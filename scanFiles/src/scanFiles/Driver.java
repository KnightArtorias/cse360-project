package scanFiles;

import javax.swing.JFrame;

import java.awt.EventQueue;

/**
 * <h1>Driver</h1>
 *
 * The driver class. Starts the application and opens the main view.
 */
public class Driver {
	private JFrame frame;
	
	/**
	 * main(String[])
	 * @param args Command line arguments
	 * 
	 * The main method. No arguments are passed in. Constructs the application's UI state.
	 */
	public static void main(String[] args){
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Driver window = new Driver();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}
	
	/**
	 * Driver()
	 * 
	 * Constructor for the Driver class. Creates the MainViewController and sets frame to be the MainView.
	 */
	public Driver() {
		MainViewController mainViewController = new MainViewController();
		frame = new MainView(mainViewController);
		
		MainViewController.view = (MainView) frame;
	}
}
