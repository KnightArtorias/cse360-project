package scanFiles;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.SwingConstants;
import java.awt.Component;
import javax.swing.Box;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Color;

/**
 *  <h1>MainView</h1>
 *  
 * The Main view of the application. Consists of:
 * 		- A browse button
 * 		- A help button
 * 		- A stats button
 * 		- A text field showing file path
 * 		- A status label
 * 
 * The purpose of this scene is to select a file for processing and to give a help screen.
 * This is the initial view in the application
 */
public class MainView extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;	
	private MainViewController controller;

	/**
	 * MainView(MainViewController)
	 * @param cont The controller for the view
	 * 
	 * Constructor for the view. Takes in its controller as an argument
	 */
	public MainView(MainViewController cont) {
		controller = cont;
		
		// Initial setup
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 500, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		// Title label
		JLabel lblPleaseSelectA = new JLabel("Please select a file");
		lblPleaseSelectA.setHorizontalAlignment(SwingConstants.CENTER);
		lblPleaseSelectA.setFont(new Font("Lucida Grande", Font.PLAIN, 24));
		contentPane.add(lblPleaseSelectA, BorderLayout.NORTH);
		
		// View that contains the text field and the browse button as well as the status label
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		
		// Status label (starts out invisible)
		final JLabel lblNewLabel = new JLabel("New label");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setBounds(67, 25, 275, 19);
		lblNewLabel.setVisible(false);
		panel.setLayout(null);
		lblNewLabel.setBackground(new Color(220, 20, 60));
		lblNewLabel.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
		panel.add(lblNewLabel);
		
		// The text field that displays the path to the file
		final JTextField textField = new JTextField();
		textField.setBounds(41, 86, 187, 26);
		panel.add(textField);
		textField.setEditable(false);
		textField.setColumns(100);
		
		// The browse button
		JButton btnBrowse = new JButton("Browse...");
		btnBrowse.setBounds(269, 85, 100, 29);
		panel.add(btnBrowse);
		
		// View that contains the help and stats buttons
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		// Stats button
		final JButton btnStats = new JButton("Stats");
		panel_1.add(btnStats, BorderLayout.EAST);
		btnStats.setOpaque(true);
		
		// Put some space on the left and right of the border layout so the CENTER doesn't stretch across the screen
		Component horizontalStrut = Box.createHorizontalStrut(40);
		contentPane.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(40);
		contentPane.add(horizontalStrut_1, BorderLayout.EAST);
		
		// Help button
		JButton btnHelp = new JButton("Help");
		panel_1.add(btnHelp, BorderLayout.WEST);
		
		controller.testForStats(btnStats, lblNewLabel);
		
		// MARK: Actions
		
		// User pressed the browse button
		btnBrowse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.browsePressed(textField, lblNewLabel, btnStats);
			}
		});
		
		// User pressed the stats button
		btnStats.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.statsPressed();
			}
		});
		
		// User pressed the help button
		btnHelp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.helpPressed();
			}
		});
	}
}
