package scanFiles;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JOptionPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.Box;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.io.FileReader;

import java.util.Scanner;
import javax.swing.SwingConstants;

/**
 * <h1>StatsView</h1>
 * 
 * The stats view of the application. Contains:
 * 		- A JList of past stats collected
 * 		- A button that displays the cumulative stats across all runs
 * 		- A title label
 * 
 */
public class StatsView extends JFrame {
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	
	private StatsViewController cont;
	private String lastSelected;

	/**
	 * StatsView(StatsViewController)
	 * 
	 * Initializes the view. Will call StatsViewController.loadListData(JList) at the end.
	 * 
	 * @param cont The controller object for this view
	 */
	public StatsView(StatsViewController cont) {
		this.cont = cont;
		
		this.cont.view = this;
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));

		JButton btnAverages = new JButton("Cumulative");
		btnAverages.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Scanner inFile = null; 
		        StringBuilder builder = new StringBuilder();

		        try {
		            inFile = new Scanner(new FileReader("./StatsAverages.txt"));
		        } catch (FileNotFoundException ioe) {
		        	 	System.err.println(ioe);
		        }

		        while(inFile.hasNextLine()){
		            String line = inFile.nextLine();
		            builder.append(line);
		            builder.append("\n");
		        }
		        JOptionPane.showMessageDialog(null, builder, "Stats", JOptionPane.PLAIN_MESSAGE);
			}
		});
		panel.add(btnAverages);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BorderLayout(0, 0));
		
		JButton btnClear = new JButton("Clear");
		panel_1.add(btnClear, BorderLayout.EAST);
		
		JLabel lblStats = new JLabel("Stats");
		lblStats.setHorizontalAlignment(SwingConstants.CENTER);
		panel_1.add(lblStats, BorderLayout.CENTER);
		
		Component horizontalStrut_2 = Box.createHorizontalStrut(75);
		panel_1.add(horizontalStrut_2, BorderLayout.WEST);
		
		Component horizontalStrut = Box.createHorizontalStrut(75);
		contentPane.add(horizontalStrut, BorderLayout.WEST);
		
		Component horizontalStrut_1 = Box.createHorizontalStrut(75);
		contentPane.add(horizontalStrut_1, BorderLayout.EAST);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		JList<String> list = new JList<String>();
		list.setToolTipText("Double-click to view stats");
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setVisibleRowCount(15);
		scrollPane.setViewportView(list);
		
		list.addListSelectionListener(new ListSelectionListener() {
            public void valueChanged(ListSelectionEvent evt) {
                if (!evt.getValueIsAdjusting()) {
                		if (list.getSelectedIndex() != -1) {
                			lastSelected = list.getModel().getElementAt(list.getSelectedIndex());
                		}
                }
            }
		});
		
		list.addMouseListener(new MouseAdapter() {
		    public void mouseClicked(MouseEvent evt) {
		        if (evt.getClickCount() == 2) {
		        		cont.displayStats(lastSelected);
		        }
		    }
		});
		
		btnClear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cont.clear(list, btnAverages);
			}
		});
		
		this.addWindowListener(new java.awt.event.WindowAdapter() {
		    public void windowClosing(java.awt.event.WindowEvent windowEvent) {
		        cont.closeStatsWindow();
		    }
		});
		
		this.cont.loadListData(list, btnAverages);
	}
}
