package scanFiles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <h1>Stats.java</h1>
 * 
 * Creates a stats object that contains several statistics calculated based on the lines from a text file.
 * The following statistics can be collected:
 * 		- Number of Lines
 * 		- Number of Blank Lines
 * 		- Number of Spaces
 * 		- Number of Words
 * 		- Average Characters Per Line
 * 		- Average Word Length
 * 		- Most Common Words
 */
public class Stats {
	// MARK: Members
	private ArrayList<String> lines 							= new ArrayList<String>();
	private LinkedHashMap<String, Integer> mostCommonWords 	= new LinkedHashMap<String, Integer>();
	private int numLines 									= 0;
	private int numBlankLines 								= 0;
	private int numSpaces 									= 0;
	private int numWords 									= 0;
	private int avgCharPerLine 								= 0;
	private int avgWordLength 								= 0;
	
	/**
	 * Stats(ArrayList)
	 * @param linesInitializer The lines from the text file
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * The initializer for stats. Takes in an ArrayList of each line in the .txt file (including blank ones).
	 * Calls the setters for each property.
	 */
	public Stats(ArrayList<String> linesInitializer, StatsAverages statsaverages) {
		lines = linesInitializer;
		statsaverages.incrementNumFilesRead();
		
		stripSpecialChars();
		
		setNumLines(statsaverages);
		setNumBlankLines(statsaverages);
		setNumSpaces(statsaverages);
		setNumWords(statsaverages);
		setAvgCharPerLine(statsaverages);
		setAvgWordLength(statsaverages);
		setMostCommonWords(statsaverages);
	}
	
	// MARK: Getters
	/**
	 * int getNumLines()
	 * 
	 * Returns the recorded number of lines from the text file.
	 * 
	 * @return The recorded number of lines from the text file
	 */
	public int getNumLines() {
		return numLines;
	}
	
	/**
	 * int getNumBlankLines()
	 * 
	 * Returns the recorded number of blank lines from the text file.
	 * 
	 * @return The recorded number of blank lines from the text file
	 */
	public int getNumBlankLines() {
		return numBlankLines;
	}
	
	/**
	 * int getNumSpaces()
	 * 
	 * Returns the recorded number of spaces from the text file.
	 * 
	 * @return The recorded number of spaces from the text file
	 */
	public int getNumSpaces() {
		return numSpaces;
	}
	
	/**
	 * int getNumWords()
	 * 
	 * Returns the recorded number of words from the text file.
	 * 
	 * @return The recorded number of words from the text file
	 */
	public int getNumWords() {
		return numWords;
	}
	
	/**
	 * int getAvgCharPerLine()
	 * 
	 * Returns the recorded average number of characters per line from the text file.
	 * 
	 * @return The recorded average number of characters per line from the text file
	 */
	public int getAvgCharPerLine() {
		return avgCharPerLine;
	}
	
	/**
	 * int getAvgWordLength()
	 * 
	 * Returns the recorded average word length from the text file.
	 * 
	 * @return The recorded average word length from the text file
	 */
	public int getAvgWordLength() {
		return avgWordLength;
	}
	
	/**
	 * int getMostCommonWords()
	 * 
	 * Returns the recorded most common words from the text file.
	 * 
	 * @return The recorded most common words from the text file
	 */
	public LinkedHashMap<String, Integer> getMostCommonWords() {
		return mostCommonWords;
	}
	
	// MARK: Setters
	/**
	 * void setNumLines(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the numLines property. Ignores blank lines in calculation.
	 */
	private void setNumLines(StatsAverages statsaverages) {
		for (int i = 0; i < lines.size(); i++) {
			if (lines.get(i).hashCode() != 0) {	// Note: string.hashCode() == 0 is the same as an empty line
				numLines++;
			}
		}
		
		statsaverages.setNumLinesAVG(numLines);
	}
	
	/**
	 * void setNumBlankLines(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the numBlankLines property. Ignores lines with characters in them.
	 */
	private void setNumBlankLines(StatsAverages statsaverages) {
		if (lines.isEmpty()){
			numBlankLines = 1;
		}
		else {
			for (int i = 0; i < lines.size(); i++) {
				if (lines.get(i).hashCode() == 0) {	// Note: string.hashCode() == 0 is the same as an empty line
					numBlankLines++;
				}
			}
		}
		
		statsaverages.setNumBlankLinesAVG(numBlankLines);
	}
	
	/**
	 * void setNumSpaces(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the numSpaces property. Removes all spaces, gets the resulting string length, then calculates the difference.
	 * Result is added to numSpaces.
	 */
	private void setNumSpaces(StatsAverages statsaverages) {
		for (int i = 0; i < lines.size(); i++) {
			String str = lines.get(i);
			String str_noSpace = str.replaceAll("\\s+","");	// Create a string where everything is smooshed together
			numSpaces = numSpaces + str.length() - str_noSpace.length();	// The number of spaces is the difference between the smooshed string and the original
		}
		
		statsaverages.setNumSpacesAVG(numSpaces);
	}
	
	/**
	 * void setNumWords(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the numWords property. Using space as a delimiter, splits the string then counts the resulting array. 
	 */
	private void setNumWords(StatsAverages statsaverages) {
		int temp = 0;
		for (int i = 0; i < lines.size(); i++) {
			if (lines.get(i).hashCode() == 0) {
				continue;	// Line is blank, not interested
			}
			
			temp += lines.get(i).split(" ").length;	// Split the string by spaces. This makes an array of words. Count that array.
		}
		
		numWords = temp;
		
		statsaverages.setNumWordsAVG(numWords);
	}
	
	/**
	 * void setAvgCharPerLine(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the avgCharPerLine property. Averages the length of each line. Includes spaces.
	 */
	private void setAvgCharPerLine(StatsAverages statsaverages) {
		int sum = 0;
		for (int i = 0; i < lines.size(); i++) {
			sum += lines.get(i).length();	// Add up all the characters in every line
		}
		
		if (lines.size() == 0) {	// Make sure we don't divide by 0
			return;
		}
		
		avgCharPerLine = sum / lines.size();	// Total num chars / num lines
		
		statsaverages.setAvgCharPerLineAVG(avgCharPerLine);
	}
	
	/**
	 * void setAvgWordLength(StatsAverages)
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 * 
	 * Sets the avgWordLength property. Splits each line using space as a delimiter, then sums the resulting substrings.
	 * Result is then divided by the number of words.
	 */
	private void setAvgWordLength(StatsAverages statsaverages) {
		int sum = 0;
		int count = getNumWords(); // Get the number of words
		for (String line : lines) {
		    String words[] = line.split(" ");
		    
		    for (String word : words) {
		    		int length = word.length();
		    		sum += length;	// This is will be the total number of characters in every word
		    }
		}
		
		if (count == 0) {		// Make sure we don't divide by 0
			avgWordLength = 0;
		} else {
			avgWordLength = sum / count;		// Total num chars in words / num words
		}
		
		statsaverages.setAvgWordLengthAVG(avgWordLength);
	}
	
	/**
	 * void setMostCommonWords(StatsAverages)
	 * 
	 * Sets the mostCommonWords property. Goes through the lines and first lowercases all characters. Then,
	 * splits each line into it's words using space as a delimeter. Then it checks each word against the LinkedHashMap.
	 * If it finds a match, it increases that entry's value by 1, otherwise it adds a new entry with an initial value of 1.
	 * Lastly, the LinkedHashMap is sorted by value.
	 * 
	 * @param statsaverages The StatsAverages object representing the averages of the statistics
	 * across all files
	 */
	private void setMostCommonWords(StatsAverages statsaverages) {
		for (String line : lines) {
			line = line.toLowerCase();
			if (line.hashCode() == 0) {	// Blank line, skip
				continue;
			}
			
			String words[] = line.split(" ");	// Get all the words
			
			for (String word : words) {
				if (mostCommonWords.containsKey(word)) {	// If the word is already in the hashMap
					mostCommonWords.put(word, mostCommonWords.get(word) + 1);		// Increase it's value by 1
				} else {
					mostCommonWords.put(word, 1);	// Otherwise add a new entry with initial value of 1
				}
			}
		}
		
		mostCommonWords = (LinkedHashMap<String, Integer>) sortByValue(mostCommonWords);	// Sort em
		
		statsaverages.setMostCommonWords(lines);
	}
	
	/**
	 * sortByValue(Map)
	 * @param map The map we want to sort on
	 * @param <K> The keys in the map
	 * @param <V> The values in the map
	 * @return A map containing the original data sorted by value
	 * 
	 * Generic sort for ordered Maps on their values. Values must be comparable. Entries must be ordered (i.e. LinkedHashMap)
	 */
	public static <K, V extends Comparable<? super V>> Map<K, V> sortByValue(Map<K, V> map) {
		// Functionally, this operation will break down the Map into it's component parts by using a functional map, 
		// then, using a collector, to collector, collects the elements back up in "reverse-sorted" order (descending). 
		// We specify that we want to sort on V (values).
		
		// It is probably easiest to read this from bottom to top
	    return map.entrySet()	// Gives a set
	              .stream()		// Get a stream of the set so we can use sorted
	              .sorted(Map.Entry.comparingByValue(Collections.reverseOrder()))	// reverseOrder is in descending order
	              .collect(Collectors.toMap(	// Standard functional map. Collect will collect the results and store them in the LinkedHashMap
	                Map.Entry::getKey, 		// Keys
	                Map.Entry::getValue, 	// Values
	                (e1, e2) -> e1, 			// Merge function (resolves collisions)
	                LinkedHashMap::new		// Return
	              ));
	    
	    // toMap breaks down the Map into Keys and Values and returns a Collector
	    // Collect will perform a reduction on the Collector (this removes duplicates)
	    // Sorted will take the result of the reduction and sort it in descending order by value
	    // stream() and entrySet() are there to convert to the appropriate type
	}
	
	/**
	 * void stripSpecialChars()
	 * 
	 * Strips the lines array of all characters that aren't alphabetic or spaces 
	 */
	private void stripSpecialChars() {
		System.out.println("Lines before stripping: " + lines);
		
		for (int i = 0; i < lines.size(); i++) {
			// Expression [^a-zA-Z ] will match will all characters that are not (^ = not) a to z, A to Z or space (note the space there)
			lines.set(i, lines.get(i).replaceAll("[^a-zA-Z ]", "")); 
		}
		
		System.out.println("Lines after stripping: " + lines);
	}
}
